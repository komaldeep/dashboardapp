import React, {Component} from 'react';
import { Container, Content, List, ListItem,CheckBox, InputGroup, Input, Button} from 'native-base';
import {StyleSheet,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Text,
    Picker,
    Item
} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/counterActions';
import { connect } from 'react-redux';
import IssueDetail from './IssueDetails';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import myTheme from '../../../Theme/Theme';
import PopupDialog from 'react-native-popup-dialog';
import Badge from 'react-native-smart-badge';

var feedbackjsonData = [{ Title:'AwesomeTrip', OverallRating:'9/10',
    OtpSource:'Expedia',Hotelname:'DemoHotel',Username:'Tom Kurchritz',
    UserCountry:'Poland',
    Date:'12/10/2016',
    UserComment:"Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry."
},

    { Title:'Like living there!!', OverallRating:'10/10',
        OtpSource:'Trip Adviser',Hotelname:'DemoHotel',Username:'Thomas',
        UserCountry:'Unite Kingdom',
        Date:'02/08/2016',
        UserComment:"Lorem Ipsum is simply dummy."
    },

    { Title:'Average', OverallRating:'8/10',
        OtpSource:'Expedia',Hotelname:'DemoHotel',Username:'Lana Milta',
        UserCountry:'Ukraine',
        Date:'12/10/2016',
        UserComment:""
    },

    { Title:'Average Hotel food was not good', OverallRating:'6.5/10',
        OtpSource:'HolidayIQ',Hotelname:'DemoHotel',Username:'Dennis Duncker',
        UserCountry:'Germany',
        Date:'15/06/2015',
        UserComment:"Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry"
    }]


var feedbackjsonData1 = { Title:'AwesomeTrip', OverallRating:'9/10',
    OtpSource:'Expedia',Hotelname:'DemoHotel',Username:'Tom Kurchritz',
    UserCountry:'Poland',
    Date:'12/10/2016',
    UserComment:"Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting  text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    Ratings:[{name:"overall-rating", rating:"4/5",}, {name:"Rooms", rating:"4/5",}, {name:"Frontdesk", rating:"4/5",}
        ,{name:"Service", rating:"4/5",},{name:"Staff Behaviour", rating:"4/5",},{name:"Food & Beverages", rating:"4/5",}]
}

class IssuesWidgit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            clicked: false,
            selected1: 'key1',
            language:'',
        };
    }


    openDrawer() {
        this.refs['myDrawer'].openDrawer();
    }

    closeDrawer(){
        this.setState({
            clicked: true,
        })
    }

    openPopupBox(){
        this.popupDialog.openDialog();
    }

    DiscussPopupBox(){
        this.DiscussupDialog.openDialog();
    }


    render() {

        var navigationView = (

            <Container style={styles.filterbackground}>
                <Content theme={myTheme} >

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Filters </Text>
                    </View>

                    <List>

                        <ListItem itemDivider>
                            <Text > Status </Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={true} onPress={this.closeDrawer.bind(this)} />
                            <Text> All </Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={true} onPress={this.closeDrawer.bind(this)} />
                            <Text> Open </Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={true} onPress={this.closeDrawer.bind(this)} />
                            <Text> Closed </Text>
                        </ListItem>



                        <ListItem itemDivider>
                            <Text> Sources </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> All </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Agoda </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Booking </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Goibibo </Text>
                        </ListItem>



                        <ListItem itemDivider>
                            <Text> Assign To- </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Neeraj </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Rakesh </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Komaldeep </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Front Desk </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Manager </Text>
                        </ListItem>



                        <ListItem itemDivider>
                            <Text> Properties </Text>
                        </ListItem>

                        <ListItem >
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text>Demo Hotel Gurgao</Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text>Demo Hotel</Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Demo Restauarnt </Text>
                        </ListItem>

                    </List>

                </Content>
            </Container>
        );

        return (

            <DrawerLayoutAndroid
                ref="myDrawer"
                drawerWidth={240}
                drawerPosition={DrawerLayoutAndroid.positions.Right}
                renderNavigationView={() => navigationView}>

                <ScrollView>

                    <TouchableOpacity style={styles.Filterbar} onPress={this.openDrawer.bind(this)}>
                        <Text style={styles.Filter}>|  Filter </Text>
                    </TouchableOpacity>


                    {
                        feedbackjsonData.map((detail , i) => {
                            return (
                                <IssueDetail Discussopenpopupbox={this.DiscussPopupBox.bind(this)} openpopupbox={this.openPopupBox.bind(this)}  navigator={this.props.navigator} detail ={detail} key={i}/>
                            )
                        })
                    }

                </ScrollView>

                <PopupDialog
                        ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                        width= {320}
                        height={350}>

                        <ScrollView style={styles.alltags}>

                            {
                                feedbackjsonData1.Ratings.map((detail1 , i) => {
                                    return (
                                        <ListItem>
                                            <Text style={styles.UserComment}> {detail1.name}-  </Text>
                                            <Badge style={styles.albadges} key={i}>
                                                {detail1.rating}
                                            </Badge>
                                        </ListItem>

                                    )
                                })
                            }

                        </ScrollView>

                    </PopupDialog>


                <PopupDialog
                    ref={(DiscussupDialog) => { this.DiscussupDialog = DiscussupDialog; }}
                    width= {320}
                    height={150}>

                    <ScrollView style={styles.Discuss}>

                        <View style={styles.inputtags}>
                            <InputGroup borderType='underline' >
                                <Input placeholder='Discuss Here' />
                            </InputGroup>
                        </View>

                        <Button block > Submit </Button>

                    </ScrollView>

                </PopupDialog>

            </DrawerLayoutAndroid>

        );
    }
}

const styles = StyleSheet.create({

    Filterbar:{
        marginRight: 10,
        marginBottom:2,
        alignItems:'flex-end',
    },

    Filter:{
        fontSize:15,
        color:"blue",
    },

    arrowstyle:{
        marginTop:14,
        marginLeft:8,
    },

    FilterHeader:{
        backgroundColor:'skyblue',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,
    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:18,
    },

    albadges:{
        backgroundColor:'rgba(102,178,255,1)',
    },

    Discuss:{
        flexWrap:"wrap",
        paddingLeft:20,
        paddingRight:20,
    },

    inputtags:{
        marginBottom:20,
        marginTop:20,
    },

    filterbackground:{
        backgroundColor:"rgba(2,59,79,0.0)",
    },

    FilterTextColor:{
        color:"#fff"
    }


})



export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(IssuesWidgit);