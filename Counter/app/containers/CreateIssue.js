import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, TouchableHighlight} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../actions/counterActions';
import { connect } from 'react-redux';
import CreateIssueHeader from '../components/Header/CreateIssueHeader';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import CreateIssueContainer from './../components/Widget/CreateIssue/CreateIssueContainer'

class CreateIssue extends Component {

    constructor(props) {
        super(props);
    }



    render() {

        return (
            <View>
                <CreateIssueHeader navigator={this.props.navigator} />
            <CreateIssueContainer/>
            </View>

        );
    }
}

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(CreateIssue);

