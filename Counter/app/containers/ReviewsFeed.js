import React, {Component} from 'react';
import { Container, Content, List, ListItem, Text } from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../actions/counterActions';
import { connect } from 'react-redux';
import ReviewsHeader from '../components/Header/FeedbackHeader';
import ReviewsFeedback from '../components/Widget/ReviewsWidget/Feedback';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';


class ReviewsFeed extends Component {

    constructor(props) {
        super(props);
    }

    Issuescontainer(){
        this.props.navigator.push({
            id:'Issuescontainer'
        })
    }

    Reviewsfeed(){
        this.props.navigator.push({
            id:'ReviewsFeed'
        })
    }

    GuestFeedback(){
       this.props.navigator.push({
        id:'GuestFeedback'
       })
    }

    openDrawer() {
        this.refs['myDrawer'].openDrawer();
    }

    render() {

        var navigationView = (

            <Container style={styles.FilterBackground}>
                <Content>

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Menu </Text>
                    </View>

                    <List>
                        <ListItem >

                            <TouchableOpacity onPress={this.Reviewsfeed.bind(this)}>
                                <Text>Reviews Feed</Text>
                            </TouchableOpacity>

                        </ListItem>

                        <ListItem>
                            <TouchableOpacity onPress={this.GuestFeedback.bind(this)}>
                                <Text>Guest Feedback</Text>
                            </TouchableOpacity>
                        </ListItem>

                        <ListItem>

                            <TouchableOpacity onPress={this.Issuescontainer.bind(this)}>
                            <Text> Issues </Text>
                            </TouchableOpacity>

                        </ListItem>

                    </List>
                </Content>
            </Container>

        );

        return (
            <DrawerLayoutAndroid
                                 ref="myDrawer"
                                 drawerWidth={300}
                                 drawerPosition={DrawerLayoutAndroid.positions.Left}
                                 renderNavigationView={() => navigationView}>

                <ReviewsHeader opentheDrawer={this.openDrawer.bind(this)} />
                <ReviewsFeedback navigator={this.props.navigator}/>


            </DrawerLayoutAndroid>
        );
    }
}


const styles = StyleSheet.create({

    FilterHeader:{
        backgroundColor:'skyblue',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,

    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:18,
    },

    FilterBackground: {
        // backgroundColor: "rgba(4,61,81,0.8)",
    }

})

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(ReviewsFeed);

