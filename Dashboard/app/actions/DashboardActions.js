

export function ReviewsFeeddata() {

    return async function (dispatch) {

            try {
                let response = await fetch('https://private-c1045-normalap.apiary-mock.com/');
                let reviewsfeeddata = await response.json();
                dispatch ({
                    type: "REVIEWSFEED",
                    payload: {
                        reviewsfeeddata
                    }
                })
            } catch (error) {
                console.error(error);
                dispatch ({
                    type: "ERROR",
                    payload: {
                        Restdata: error
                    }
                })
            }

    }
}


export function IssuesList() {

    return async function (dispatch) {
        try {
            let response = await fetch('https://private-1250c-issueslist.apiary-mock.com/');
            let Issuesreviewsfeeddata = await response.json();

            dispatch ({
                type: "ISSUESLIST",
                payload: {
                    Issuesreviewsfeeddata
                }
            })

        } catch (error) {
            console.error(error);
            dispatch ({
                type: "ERROR",
                payload: {
                    Restdata: error
                }
            })
        }

    }
}


export function GuestfeedbackList() {

    return async function (dispatch) {
        try {
            let response = await fetch('https://private-c1045-normalap.apiary-mock.com/');
            let guestfeedbackdata = await response.json();

            dispatch ({
                type: "GUESTFEEDBACKLIST",
                payload: {
                    guestfeedbackdata
                }
            })

        } catch (error) {
            console.error(error);
            dispatch ({
                type: "ERROR",
                payload: {
                    Restdata: error
                }
            })
        }

    }
}


