

const initialState = {
    count: 0,
    Restdata: false,
    ReviewsFeed: false,
    Issueslist:false,
    GuestList:false,
};

export default function counter(state = initialState, action = {}) {
  switch (action.type) {


      case "RESTDATA":
          return {
              ...state,
              Restdata: action.payload
          };

      case "REVIEWSFEED":
          return {
              ...state,
              ReviewsFeed: action.payload
          }

      case "ERROR":
          return {
              ...state,
              Error: action.payload
          }

      case "ISSUESLIST":
          return {
              ...state,
              Issueslist:action.payload
          }

      case "GUESTFEEDBACKLIST":
          return {
              ...state,
              GuestList:action.payload
          }

    default:
      return state;
  }
}
