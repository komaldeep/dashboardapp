import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, TouchableHighlight} from 'react-native';
import { Container, Header, Title, Button, Icon } from 'native-base';

export default class CreateIssueHeader extends Component {

    constructor(props) {
        super(props);
    }


    backPageButtonheader(){
        this.props.navigator.pop();
    }

    render() {
        return (
            <Header>

                <Button transparent onPress={this.backPageButtonheader.bind(this)}>
                    <Icon name='ios-arrow-back' />
                </Button>

                <Title>
                    Create Issue
                </Title>

            </Header>
        );
    }
}




