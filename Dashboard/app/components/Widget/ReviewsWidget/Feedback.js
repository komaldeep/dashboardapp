import React, {Component} from 'react';
import { Container, Content, List, ListItem,CheckBox , Icon} from 'native-base';
import {StyleSheet,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Text,
    Picker,
    Item,
    ListView
     } from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
import ReviewsDetail from './ReviewsDetail';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import myTheme from '../../../Theme/Theme';
import PopupDialog from 'react-native-popup-dialog';
import Badge from 'react-native-smart-badge';
import Spinner from 'react-native-loading-spinner-overlay';
var Realm = require('realm');
import Dimensions from 'Dimensions';



var feedbackjsonData1 = { Title:'AwesomeTrip', OverallRating:'9/10',
    OtpSource:'Expedia',Hotelname:'DemoHotel',Username:'Tom Kurchritz',
    UserCountry:'Poland',
    Date:'12/10/2016',
    UserComment:"Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting  text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    Ratings:[{name:"overall-rating", rating:"4/5",}, {name:"Rooms", rating:"4/5",}, {name:"Frontdesk", rating:"4/5",}
        ,{name:"Service", rating:"4/5",},{name:"Staff Behaviour", rating:"4/5",},{name:"Food & Beverages", rating:"4/5",}]
}

class Feedback extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            clicked: false,
            selected1: 'key1',
            language:'',
            realm:'',
            visible: true,
            loading:4,
        };
    }



    openDrawer() {
    this.refs['myDrawer'].openDrawer();
    }

    closeDrawer(){
        this.setState({
            clicked: true,
        })
    }


    openPopupBox(){
        this.popupDialog.openDialog();
    }


    handleScroll(event){
        var windowHeight = Dimensions.get('window').height,
            height = event.nativeEvent.contentSize.height,
            offset = event.nativeEvent.contentOffset.y;
        if( windowHeight + offset >= height ){

            let loadingnumber = this.state.loading;
            let addingnumber = 4;
            this.setState({
                loading: loadingnumber+ addingnumber,
            })

        }
    }



    render() {

        let navigationView = (
            <Container>
                <Content theme={myTheme}>

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Filters </Text>
                    </View>


                    <List>
                        <ListItem>

                            <Picker
                                selectedValue={this.state.language}
                                onValueChange={(lang) => this.setState({language: lang})}>
                                <Picker.Item label="Februry 2017" value="Februry 2017" />
                                <Picker.Item label="January 2017" value="January 2017" />
                                <Picker.Item label="December 2016" value="December 2017" />
                                <Picker.Item label="November 2016" value="November 2017" />
                            </Picker>


                        </ListItem>


                        <ListItem itemDivider>
                            <Text> Properties </Text>
                        </ListItem>

                        <ListItem >
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text>Demo Hotel Gurgao</Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text>Demo Hotel</Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Demo Restauarnt </Text>
                        </ListItem>

                        <ListItem itemDivider>
                            <Text> OTA </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> All </Text>
                        </ListItem>
                        <ListItem>
                             <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Agoda </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Booking </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Goibibo </Text>
                        </ListItem>

                    </List>
                </Content>
            </Container>
        );

        const { ReviewsFeed } = this.props.state;

        var realm = new Realm({
            schema: [{name: 'Feedbackdata', properties: {feedbackappdata: 'string'}}]
        });

        let showdata;

        let realobjectlength

        if(ReviewsFeed == false){

            let backupdata = realm.objects('Feedbackdata');
            realobjectlength =  <View style={styles.ViewLoading}>
                  <Text style={styles.loading}>Loading... </Text>
            </View>

            if(realm.objects('Feedbackdata').length == 1) {

                var stringtoarray = backupdata[0].feedbackappdata;

                var feedbackobjects = JSON.parse(stringtoarray);

                showdata =
                    feedbackobjects.map((detail, i) => {
                        return (
                            <ReviewsDetail openpopupbox={this.openPopupBox.bind(this)} navigator={this.props.navigator}
                                           detail={detail} key={i}/>
                        )
                    })

            }

        }

        else {

            let stringyfyobject = JSON.stringify(ReviewsFeed.reviewsfeeddata);

            realm.write(() => {
                let backupdata = realm.create('Feedbackdata', {feedbackappdata: stringyfyobject});
            });

            let objectlength = realm.objects('Feedbackdata').length;

            if(objectlength > 1){
                realm.write(() => {
                    let backupdata = realm.create('Feedbackdata', {feedbackappdata: stringyfyobject});
                    let allobjects = realm.objects('Feedbackdata');
                    realm.delete(allobjects);
                    realm.create('Feedbackdata', {feedbackappdata: stringyfyobject});
                });
            }

            realobjectlength = <View></View>;

            let ReviewsFeedarray = [];
            for (let i in ReviewsFeed.reviewsfeeddata) {
                ReviewsFeedarray.push(ReviewsFeed.reviewsfeeddata[i]);
            }

            let loading = this.state.loading;

            let slicearray = ReviewsFeedarray.splice(0,loading)

            showdata =
                slicearray.map((detail , i) => {
                return (
                    <ReviewsDetail openpopupbox={this.openPopupBox.bind(this)} navigator={this.props.navigator} detail ={detail} key={i} />
                )
            })

        }


        return (

            <DrawerLayoutAndroid
                                 ref="myDrawer"
                                 drawerWidth={240}
                                 drawerPosition={DrawerLayoutAndroid.positions.Right}
                                 renderNavigationView={() => navigationView}>

                <ScrollView
                    showsVerticalScrollIndicator ={true}
                    onScroll={this.handleScroll.bind(this)}
                >

                     {realobjectlength}
                        <TouchableOpacity style={styles.Filterbar} onPress={this.openDrawer.bind(this)}>
                            <Text style={styles.Filter}>|  Filter </Text>
                        </TouchableOpacity>
                     {showdata}

                </ScrollView>

                <PopupDialog
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    width= {320}
                    height={350}>

                    <ScrollView style={styles.alltags}>

                        {
                            feedbackjsonData1.Ratings.map((detail1 , i) => {
                                return (
                                    <ListItem key={i}>
                                        <Text style={styles.UserComment}> {detail1.name}-  </Text>
                                        <Badge style={styles.albadges} key={i}>
                                            {detail1.rating}
                                        </Badge>
                                    </ListItem>

                                )
                            })
                        }

                    </ScrollView>

                </PopupDialog>

            </DrawerLayoutAndroid>

        );
    }
}

const styles = StyleSheet.create({

    Filterbar:{
        marginRight: 10,
        marginBottom:2,
        alignItems:'flex-end',
    },

    Filter:{
        fontSize:15,
        color:"blue",
    },

    arrowstyle:{
        marginTop:14,
        marginLeft:8,
    },

    FilterHeader:{
        backgroundColor:'skyblue',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,
    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:18,
    },

    albadges:{
        backgroundColor:'rgba(102,204,204,1)',
    },

    loading:{
        alignItems:"center",
        color:"#fff",
        fontSize:15,
    },

    ViewLoading:{
        flex:1,
        alignItems:"center",
        backgroundColor:"rgba(0, 0, 0, 0.3)",
        height:35,
        justifyContent: 'center',
    },

    locationiconsize:{
        fontSize:15,
        color:"#66CCCC"
    }
})



export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(Feedback);

