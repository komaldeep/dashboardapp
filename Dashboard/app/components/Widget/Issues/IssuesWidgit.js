import React, {Component} from 'react';
import { Container, Content, List, ListItem,CheckBox, InputGroup, Input, Button} from 'native-base';
import {StyleSheet,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Text,
    Picker,
    Item
} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/DashboardActions';
import { connect } from 'react-redux';
import IssueDetail from './IssueDetails';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import myTheme from '../../../Theme/Theme';
import PopupDialog from 'react-native-popup-dialog';
import Badge from 'react-native-smart-badge';
import Dimensions from 'Dimensions';


var feedbackjsonData1 = {Title:'AwesomeTrip', OverallRating:'9/10',
    OtpSource:'Expedia',Hotelname:'DemoHotel',Username:'Tom Kurchritz',
    UserCountry:'Poland',
    Date:'12/10/2016',
    UserComment:"Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting  text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    Ratings:[{name:"overall-rating", rating:"4/5",}, {name:"Rooms", rating:"4/5",}, {name:"Frontdesk", rating:"4/5",}
        ,{name:"Service", rating:"4/5",},{name:"Staff Behaviour", rating:"4/5",},{name:"Food & Beverages", rating:"4/5",}]
}

class IssuesWidgit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            clicked: false,
            selected1: 'key1',
            language:'',
            loading:4,
        };
    }


    openDrawer() {
        this.refs['myDrawer'].openDrawer();
    }

    closeDrawer(){
        this.setState({
            clicked: true,
        })
    }

    openPopupBox(){
        this.popupDialog.openDialog();
    }

    DiscussPopupBox(){
        this.DiscussupDialog.openDialog();
    }

    handleScroll(event){
        var windowHeight = Dimensions.get('window').height,
            height = event.nativeEvent.contentSize.height,
            offset = event.nativeEvent.contentOffset.y;
        if( windowHeight + offset >= height ){

            let loadingnumber = this.state.loading;
            let addingnumber = 4;
            this.setState({
                loading: loadingnumber+ addingnumber,
            })

        }
    }


    render() {



        var navigationView = (
            <Container style={styles.filterbackground}>
                <Content theme={myTheme} >

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Filters </Text>
                    </View>

                    <List>

                        <ListItem itemDivider>
                            <Text > Status </Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={true} onPress={this.closeDrawer.bind(this)} />
                            <Text> All </Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={true} onPress={this.closeDrawer.bind(this)} />
                            <Text> Open </Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={true} onPress={this.closeDrawer.bind(this)} />
                            <Text> Closed </Text>
                        </ListItem>



                        <ListItem itemDivider>
                            <Text> Sources </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> All </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Agoda </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Booking </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Goibibo </Text>
                        </ListItem>



                        <ListItem itemDivider>
                            <Text> Assign To- </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Neeraj </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Rakesh </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Komaldeep </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Front Desk </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Manager </Text>
                        </ListItem>



                        <ListItem itemDivider>
                            <Text> Properties </Text>
                        </ListItem>

                        <ListItem >
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text>Demo Hotel Gurgao</Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text>Demo Hotel</Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Demo Restauarnt </Text>
                        </ListItem>

                    </List>

                </Content>
            </Container>
        );

        let realm = new Realm({
            schema: [{name: 'Issuebackdata', properties: {Issuebackappdata: 'string'}}]
        });


        const { Issueslist } = this.props.Issues;
        let ListShow;
        let realobjectlength;

        if(Issueslist == false){


            let backupdata = realm.objects('Issuebackdata');
            realobjectlength =  <View style={styles.ViewLoading}>
                <Text style={styles.loading}> Loading... </Text>
            </View>

            if(realm.objects('Issuebackdata').length == 1) {

                var stringtoarray = backupdata[0].Issuebackappdata;

                var feedbackobjects = JSON.parse(stringtoarray);

                ListShow =
                    feedbackobjects.map((detail, i) => {
                        return (
                            <IssueDetail Discussopenpopupbox={this.DiscussPopupBox.bind(this)}
                                         openpopupbox={this.openPopupBox.bind(this)}
                                         navigator={this.props.navigator}
                                         detail ={detail} key={i} />

                        )
                    })

            }

        }

        else {



            let stringyfyobject = JSON.stringify(Issueslist.Issuesreviewsfeeddata);

            realm.write(() => {
                let backupdata = realm.create('Issuebackdata', {Issuebackappdata: stringyfyobject});
            });

            let objectlength = realm.objects('Issuebackdata').length;

            if(objectlength > 1){
                realm.write(() => {
                    let backupdata = realm.create('Issuebackdata', {Issuebackappdata: stringyfyobject});
                    let allobjects = realm.objects('Issuebackdata');
                    realm.delete(allobjects);
                    realm.create('Issuebackdata', {Issuebackappdata: stringyfyobject});
                });
            }

            realobjectlength = <View></View>;

            let ReviewsIssueslist = [];
            for (let i in Issueslist.Issuesreviewsfeeddata) {
                ReviewsIssueslist.push(Issueslist.Issuesreviewsfeeddata[i]);
            }

            let loading = this.state.loading;
            let slicearray = ReviewsIssueslist.splice(0,loading)

            ListShow =
                slicearray.map((detail , i) => {
                return (
                    <IssueDetail Discussopenpopupbox={this.DiscussPopupBox.bind(this)} openpopupbox={this.openPopupBox.bind(this)}  navigator={this.props.navigator} detail ={detail} key={i}/>
                )
                })

        }


        return (

            <DrawerLayoutAndroid
                ref="myDrawer"
                drawerWidth={240}
                drawerPosition={DrawerLayoutAndroid.positions.Right}
                renderNavigationView={() => navigationView}>

                <ScrollView
                    showsVerticalScrollIndicator ={true}
                    onScroll={this.handleScroll.bind(this)}
                >

                    {realobjectlength}
                    <TouchableOpacity style={styles.Filterbar} onPress={this.openDrawer.bind(this)}>
                        <Text style={styles.Filter}>|  Filter </Text>
                    </TouchableOpacity>

                    {ListShow}

                </ScrollView>

                <PopupDialog
                        ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                        width= {320}
                        height={350}>

                        <ScrollView style={styles.alltags}>

                            {
                                feedbackjsonData1.Ratings.map((detail1 , i) => {
                                    return (
                                        <ListItem key={i}>
                                            <Text style={styles.UserComment}> {detail1.name}-  </Text>
                                            <Badge style={styles.albadges} key={i} >
                                                {detail1.rating}
                                            </Badge>
                                        </ListItem>

                                    )
                                })
                            }

                        </ScrollView>

                </PopupDialog>


                <PopupDialog
                    ref={(DiscussupDialog) => { this.DiscussupDialog = DiscussupDialog; }}
                    width= {320}
                    height={150}>

                    <ScrollView style={styles.Discuss}>

                        <View style={styles.inputtags}>
                            <InputGroup borderType='underline' >
                                <Input placeholder='Discuss Here' />
                            </InputGroup>
                        </View>

                        <Button block > Submit </Button>

                    </ScrollView>

                </PopupDialog>

            </DrawerLayoutAndroid>

        );
    }
}

const styles = StyleSheet.create({

    Filterbar:{
        marginRight: 10,
        marginBottom:2,
        alignItems:'flex-end',
    },

    Filter:{
        fontSize:15,
        color:"blue",
    },

    arrowstyle:{
        marginTop:14,
        marginLeft:8,
    },

    FilterHeader:{
        backgroundColor:'skyblue',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,
    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:18,
    },

    albadges:{
        backgroundColor:'rgba(102,178,255,1)',
    },

    Discuss:{
        flexWrap:"wrap",
        paddingLeft:20,
        paddingRight:20,
    },

    inputtags:{
        marginBottom:20,
        marginTop:20,
    },

    filterbackground:{
        backgroundColor:"rgba(2,59,79,0.0)",
    },

    FilterTextColor:{
        color:"#fff"
    },

    loading:{
        alignItems:"center",
        color:"#fff",
        fontSize:15,
    },

    ViewLoading:{
        flex:1,
        alignItems:"center",
        backgroundColor:"rgba(0, 0, 0, 0.3)",
        height:35,
        justifyContent: 'center',
    },


})


export default connect(Issues => ({
        Issues: Issues.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(IssuesWidgit);